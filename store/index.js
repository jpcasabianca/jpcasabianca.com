import Vuex from 'vuex';

import dataJson from '~/static/data/data.json';

const createStore = () => {
  return new Vuex.Store({
    state: {
      counter: 0,
      menu: true,
      thirdMenu: false,
      mainMenu: dataJson.main_menu,
      contactSmallModal: dataJson.contact_small_modal,
    },
    mutations: {
      increment (state) {
        state.counter++;
      },
      toggle_main_menu ( state ) {
        state.menu = !state.menu;
      },
      toggle_menu (state, payload) {
        if(payload.action == 'open') {
          for (var i = 0; i < state.mainMenu.length; i++) {
            if (i == payload.index && state.mainMenu[i].sub_menu.length > 0) {
              state.thirdMenu = true;
            } else if (i == payload.index && state.mainMenu[i].sub_menu.length == 0) {
              state.thirdMenu = false;
            }
          }
        } else {
          state.thirdMenu = false;
        }
      },
      activate_menu_item( state, payload ) {
        for (var i = 0; i < state.mainMenu.length; i++) {
          if (i == payload.index) {
            state.mainMenu[i].status = 'active';
          } else {
            state.mainMenu[i].status = '';
          }
        }
      },
      activate_sub_menu_item( state, payload ) {
        for (var i = 0; i < state.mainMenu.length; i++) {
          if (state.mainMenu[i].sub_menu.length > 0) {
            for (var j = 0; j < state.mainMenu[i].sub_menu.length; j++) {
              if (i == payload.index && j == payload.index2) {
                state.mainMenu[i].sub_menu[j].status = 'active';
              } else {
                state.mainMenu[i].sub_menu[j].status = '';
              }
            }
          }
        }
      },
      toggle_modal_window( state, payload ) {
        if(payload.status == 'active') {
          state.contactSmallModal.status = '';
        } else {
          state.contactSmallModal.status = 'active';
        }
      },
    },
    actions: {
      toggle_information_in_third_menu ( context, payload ) {
        context.commit('toggle_menu', payload);
        context.commit('activate_menu_item', payload);
      }
    },
  });
};

export default createStore;
